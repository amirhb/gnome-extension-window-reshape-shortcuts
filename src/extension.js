
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Lang = imports.lang;



let text, button;

function _hideHello() {
    Main.uiGroup.remove_actor(text);
    text = null;
}

function _showHello() {
    if (!text) {
        text = new St.Label({ style_class: 'helloworld-label', text: "Hello!" });
        Main.uiGroup.add_actor(text);
    }

    text.opacity = 255;

    let monitor = Main.layoutManager.primaryMonitor;

    text.set_position(monitor.x + Math.floor(monitor.width / 2 - text.width / 2),
                      monitor.y + Math.floor(monitor.height / 2 - text.height / 2));

    Tweener.addTween(text,
                     { opacity: 0,
                       time: 2,
                       transition: 'easeOutQuad',
                       onComplete: _hideHello });
}

function init() {
    button = new St.Bin({ style_class: 'panel-button',
                          reactive: true,
                          can_focus: true,
                          x_fill: true,
                          y_fill: false,
                          track_hover: true });
    let icon = new St.Icon({ icon_name: 'system-run-symbolic',
                             style_class: 'system-status-icon' });

    button.set_child(icon);
    button.connect('button-press-event', _showHello);
}

function enable() {
    Main.panel._rightBox.insert_child_at_index(button, 0);
    
    Main.overview._specialToggle = function (evt) {
    	_showHello();
    };
    Main.wm.setCustomKeybindingHandler('toggle-overview',
        Shell.ActionMode.NORMAL | Shell.ActionMode.OVERVIEW,
        Lang.bind(Main.overview, Main.overview._specialToggle));

    log('[EXTENSION_LOG]', 'going going')
    
//	var propValue;
//	for(var propName in theThing) {
//	    propValue = Main.wm[propName]
//        log('[EXTENSION_LOG]', propName,propValue);
//	}

	Main.wm.addKeybinding(
            'key',
            55,
            Meta.KeyBindingFlags.NONE,
            Shell.ActionMode.NORMAL,
            Main.overview._specialToggle
        );
    log('[EXTENSION_LOG]', 'gone')



    
}

function disable() {
    Main.wm.setCustomKeybindingHandler('toggle-overview',
            Shell.ActionMode.NORMAL, Lang.bind(Main.overview, Main.overview.toggle));
    delete Main.overview._specialToggle;

    Main.panel._rightBox.remove_child(button);
}
